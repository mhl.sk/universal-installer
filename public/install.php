<?php 

    require __DIR__ . "/../vendor/autoload.php";

    class Installer {

        public function __construct()
        {
            if($_SERVER['REQUEST_METHOD'] == "GET") {
                $this->showInstallationPrompt();
            } else if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $this->installApplication();
            } else {
                die("Method not allowed");
            }
        }

        private function showInstallationPrompt()
        {
            include __DIR__ . "/../installer/views/install.php";
        }

        private function installApplication()
        {
            if(
                isset($_POST['host'])
                && isset($_POST['username'])
                && isset($_POST['password'])
                && isset($_POST['database'])
                && isset($_POST['port'])
                && $this->verifyDatabaseCredentials(
                    $_POST['host'], 
                    $_POST['username'], 
                    $_POST['password'],
                    $_POST['database'],
                    $_POST['port']
                )
            ){
                try {

                    /** Connect to database */
                    $mysqli = new mysqli(
                        $_POST['host'], 
                        $_POST['username'], 
                        $_POST['password'],
                        $_POST['database'],
                        $_POST['port']
                    );

                    /** Recreate database */
                    $mysqli->query('SET foreign_key_checks = 0');
                    if ($result = $mysqli->query("SHOW TABLES")){
                        while($row = $result->fetch_array(MYSQLI_NUM)){
                            $mysqli->query('DROP TABLE IF EXISTS '.$row[0]);
                        }
                    }
                    $mysqli->query('SET foreign_key_checks = 1');

                    /** Import SQL File */
                    $mysqli->multi_query(file_get_contents(__DIR__ . "/../installer/database/database.sql"));

                    /** Include success view */
                    include __DIR__ . "/../installer/views/success.php";

                    /** Remove files in directory */
                    foreach(array_slice(scandir(__DIR__ . "/../installer"), 2) as $folder) {
                        foreach(array_slice(scandir(__DIR__ . "/../installer/" . $folder), 2) as $file) {
                            unlink(__DIR__ . "/../installer/" . $folder . "/" . $file);
                        }
                        rmdir(__DIR__ . "/../installer/" . $folder);
                    }

                    /** Remove directory */
                    rmdir(__DIR__ . "/../installer");

                    /** Create .env file */
                    file_put_contents(__DIR__ . "/../.env", "DB_HOST=" . $_POST['host'] . "\n" . "DB_PORT=" . $_POST['port'] . "\n" . "DB_NAME=" . $_POST['database'] . "\n" . "DB_USERNAME=" . $_POST['username'] . "\n" . "DB_PASSWORD=" . $_POST['password']);

                    /** Remove installer file */
                    unlink(__DIR__ . "/install.php");

                    /** Run content import fuction */
                    $this->importContent();

                } catch(Exception $e) {
                    header("location: /install.php?installationFailed=1&host=" . $_POST['host'] . "&username=" . $_POST['username'] . "&password=" . $_POST['password'] . "&database=" . $_POST['database'] . "&port=" . $_POST['port']);
                } 
            } else {
                header("location: /install.php?error=1&host=" . $_POST['host'] . "&username=" . $_POST['username'] . "&password=" . $_POST['password'] . "&database=" . $_POST['database'] . "&port=" . $_POST['port']);
            }
        }

        private function verifyDatabaseCredentials($host, $username, $password, $database, $port)
        {
            try {
                new MySQLi($host, $username, $password, $database, $port);
            } catch(Exception $e) {
                return false;
            }
            return true;
        }

        private function importContent()
        {

            // Example
            file_put_contents(__DIR__ . "/index.php", "Time to make something epic!");

        }

    } 
    
    if(!file_exists(__DIR__ . "/../.env")) {
        new Installer();
    } else {
        die("Application has already been installed");
    }
    
?>