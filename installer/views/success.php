<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Page title -->
    <title>Install project</title>

    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link 
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
        crossorigin="anonymous"
    >

    <!-- Styling overwites -->
    <style>
        .card, .card-header, .card-body, .card-footer {
            border-color: var(--bs-border-color);
        }
    </style>

</head>
<body>
    <div class="container pt-3">
        <div class="col-12 col-lg-6 mx-auto">
            <div class="card">
                <div class="card-header p-3 bg-white">
                    Installation succesfull
                </div>
                <div class="card-body bg-light">
                    Application has been succesfully installed. You can start using it now.
                </div>
                <div class="card-footer p-3 bg-white">
                    <a href="/">
                        <button class="btn btn-secondary">Finish</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>