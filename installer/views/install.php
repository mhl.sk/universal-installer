<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Page title -->
    <title>Install project</title>

    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap -->
    <link 
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" 
        crossorigin="anonymous"
    >

    <!-- Styling overwites -->
    <style>
        .card, .card-header, .card-body, .card-footer {
            border-color: var(--bs-border-color);
        }
    </style>

</head>
<body>
    <div class="container pt-3">
        <div class="col-12 col-lg-6 mx-auto">
            <form action="" method="POST">
                <div class="card">
                    <div class="card-header p-3 bg-white">
                        Install application
                    </div>
                    <div class="card-body bg-light">
                        <div class="form-floating mb-3">
                            <input 
                                type="text" 
                                class="form-control" 
                                id="host"
                                name="host" 
                                placeholder="Host"
                                value="<?php echo isset($_GET['host']) ? $_GET['host'] : 'localhost' ?>"
                                required
                            >
                            <label for="host">
                                Host
                            </label>
                        </div>
                        <div class="form-floating mb-3">
                            <input 
                                type="text" 
                                class="form-control" 
                                id="port"
                                name="port" 
                                placeholder="Port"
                                value="<?php echo isset($_GET['port']) ? $_GET['port'] : '3306' ?>"
                                required
                            >
                            <label for="port">
                                Port
                            </label>
                        </div>
                        <div class="form-floating mb-3">
                            <input 
                                type="text" 
                                class="form-control" 
                                id="username"
                                name="username" 
                                placeholder="Username"
                                value="<?php echo isset($_GET['username']) ? $_GET['username'] : 'root' ?>"
                                required
                            >
                            <label for="username">
                                Username
                            </label>
                        </div>
                        <div class="form-floating mb-3">
                            <input 
                                type="text" 
                                class="form-control" 
                                id="password"
                                name="password" 
                                placeholder="Password"
                                value="<?php echo isset($_GET['password']) ? $_GET['password'] : '' ?>"
                            >
                            <label for="password">
                                Password
                            </label>
                        </div>
                        <div class="form-floating">
                            <input 
                                type="text" 
                                class="form-control" 
                                id="database"
                                name="database" 
                                placeholder="Database"
                                value="<?php echo isset($_GET['database']) ? $_GET['database'] : '' ?>"
                                required
                            >
                            <label for="database">
                                Database
                            </label>
                        </div>
                    </div>
                    <div class="card-footer p-3 bg-white">
                        <button class="btn btn-secondary">Install</button>
                        <?php if(isset($_GET['error'])) { ?>
                            <span class="ms-2">Incorrect login credentials</span> 
                            <script>
                                window.history.pushState({}, document.title, "/install.php");
                            </script>
                        <?php } ?>
                        <?php if(isset($_GET['installationFailed'])) { ?>
                            <span class="ms-2">Installation failed, try again</span> 
                            <script>
                                window.history.pushState({}, document.title, "/install.php");
                            </script>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>